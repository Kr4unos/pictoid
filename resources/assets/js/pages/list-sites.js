$( document ).ready( function() {
    var $grid = $( '.grid-sites' ), currentLang, langs, $sizer, $linksMenu, anchor;
    if ( $grid.length ) {
        langs = [];
        currentLang = '';
        $sizer = $grid.find( '.shuffle__sizer' );
        $linksMenu = $( '.select-sites-by-lang' );

        $grid.shuffle( {
            itemSelector: '.box-site',
            sizer: $sizer
        } );

        $( '.main-search' ).on( 'keyup change', function() {
            myShuffle();
        } );

        $linksMenu.each( function() {
            langs.push( $( this ).data( 'lang' ) );
            $( this ).on( 'click', function( e ) {
                e.preventDefault();
                if ( $( this ).data( 'lang' ) !== '' ) {
                    anchor = '#games-' + $( this ).data( 'lang' );
                } else {
                    anchor = '';
                }

                history.pushState( {}, document.title, document.location.href.replace( location.hash, '' ) + anchor );
                checkUrl();
            } );
        } );
        function checkUrl( first ) {
            if ( typeof ( first ) == 'undefined' ) {
                first = false;
            }
            $linksMenu.parent().removeClass( 'active' );
            var l = document.location.href.substr( document.location.href.indexOf( '#games-' ) + 7 );
            if ( l == '' && first ) {
                var language = navigator.my_language().substring( 0, 2 );
                if ( $.inArray( language, langs ) != -1 ) {
                    history.pushState( {}, document.title, document.location.href.replace( location.hash, '' ) + '#games-' + language );
                    checkUrl();
                    return;
                }
            }
            if ( $.inArray( l, langs ) == -1 ) {
                l = '';
            }

            $linksMenu.filter( '[data-lang="' + l + '"]' ).parent().addClass( 'active' );
            currentLang = l;
            myShuffle();
        }

        function myShuffle() {
            var search = $( '.main-search' ).val().toLowerCase();
            $grid.shuffle( 'shuffle', function( $el, shuffle ) {
                var text = $.trim( $el.find( '.box-title' ).text() ).toLowerCase();
                var checkLang = false;
                var checkSearch = false;

                /** Vérification que le site appartient à la bonne langue **/
                if ( currentLang == '' ) {
                    checkLang = true;
                } else {
                    checkLang = $el.data( 'lang' ) == currentLang || $el.data( 'lang' ) == 'all';
                }

                /** Vérification que le site correspond au texte de la recherche **/
                checkSearch = text.indexOf( search ) !== -1;

                return checkLang && checkSearch;
            } );
        }

        checkUrl( true );
    }
} );
