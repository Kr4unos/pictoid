$( document ).ready(function() {
    $('#maj-contacts').on('click', function (e) {

        $.post( laroute.route('contacts-id'), function( contacts ) {
            $.each(contacts, function (k,contact) {
                contact = contact.user;
                var img = '';
                if(contact.picture) {
                    img = '<img alt="User Image" class="img-circle" src="'+contact.picture.url+'" alt="'+contact.name+'" style="width:45px;">&nbsp;';
                }
                $('<li>'+img+contact.name+'&nbsp;<i class="fa fa-fw fa-refresh" style="cursor: pointer"></i></li>').appendTo('ul.result').on('click', function (e) {
                    e.preventDefault();
                    var $this = $(this);
                    $this.find('i').addClass('fa-spin');
                    $.post(laroute.route('add-contact-twino'),{ id: contact.id},function(response) {
                        var $i = $this.find('i');
                        $i.removeClass('fa-spin')
                        if(response == 'OK'){
                            $i.css('color', 'green');
                            $i.after('Mise à jour terminée');
                        }else{
                            $i.css('color', 'red');
                            $i.after(response);
                        }
                    });
                });
            });
        });
    });
});
