$( document ).ready( function() {
    function checkJob( routeName ) {
        var $loadJob;
        $.post( laroute.route( routeName ), function( updating ) {
            if ( updating ) {
                setTimeout( function() {
                    checkJob( routeName );
                }, 5000 );
            } else {
                $loadJob = $( '.load-job[data-route="' + routeName + '"]' );
                $loadJob.find( 'span' ).css( 'display', 'block' );
                $loadJob.find( 'i' ).removeClass( 'fa-spin' );
            }
        } );
    }

    $( '.load-job' ).each( function() {
        $( this ).find( 'a' ).on( 'click', function( e ) {
            e.preventDefault();
            location.reload();
        } );

        checkJob( $( this ).data( 'route' ) );
    } );
} );
