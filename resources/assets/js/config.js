Lang.setLocale( $( 'body' ).data( 'locale' ) );
laroute.prefix = Lang.getLocale();
$.ajaxSetup(
    {
        headers: {
            'X-CSRF-TOKEN': $( 'meta[name="csrf-token"]' ).attr( 'content' ),
            'X-PICTOID-LOCALE': Lang.getLocale()
        },
        error: function( jqXHR, textStatus, errorThrown ) {
            var error, message, buttons;
            if ( typeof( jqXHR.responseJSON ) != 'undefined' && typeof( jqXHR.responseJSON.error ) != 'undefined' ) {
                error = jqXHR.responseJSON.error;
                message = error.message ? error.message : 'error';
                buttons = {};
                if ( error.exception == 'TwinoidAPIException' && error.message == 'invalid_token' ) {
                    message = 'Votre session twinoid a expirée. Veuillez vous reconnecter.';
                    buttons = {
                        'reconnect': {
                            className: 'btn-outline',
                            label: 'Me reconnecter',
                            callback: function() {
                                window.location.href = laroute.url( 'auth/login', [] );
                            }
                        }
                    };
                }
                bootbox.dialog( {
                    buttons: buttons,
                    className: 'modal-danger',
                    title: 'Error',
                    message: message
                } );
            } else {
                console.error( 'jqXHR', jqXHR );
                console.error( 'textStatus', textStatus );
                console.error( 'errorThrown', errorThrown );
            }
        }
    }
);

