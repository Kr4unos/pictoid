<?php

return [
    'nb_stats' => ':nb statistical|:nb statistics',
    'nb_recompenses' => ':nb win|:nb wins',
    'nb_points' => ':nb point|:nb points',
    'nb_joueurs' => ':nb player|:nb players',
    'tous_les_jeux' => 'all games',
    'pictos' => 'Details',
    'connexion' => 'sign in',
    'deconnexion' => 'log out',
    'non_officiel' => 'Twinoid Application designed by a player for players. Most images are property of Motion Twin.',
    'categories' => 'Categories',
    'recherche' => 'Search',
    'toutes-categories' => 'all',
    'ajouter' => 'add',
    'ajouter-categorie' => 'Add a category',
    'editer-categorie' => 'Edit a category',
    'nom-categorie' => 'Name',
    'enregistrer' => 'Save',
    'maj-en-cours' => 'Updating, come back in a moment.',
];
