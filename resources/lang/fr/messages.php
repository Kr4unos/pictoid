<?php

return [

    'nb_stats'          => ':nb statistique|:nb statistiques',
    'nb_recompenses'    => ':nb récompense|:nb récompenses',
    'nb_points'         => ':nb point|:nb points',
    'nb_joueurs'        => ':nb joueur|:nb joueurs',
    'tous_les_jeux'     => 'tous les jeux',
    'pictos'            => 'pictos',
    'connexion'         => 'connexion',
    'deconnexion'       => 'déconnexion',
    'non_officiel'      => 'Application Twinoïd conçue par un joueur pour les joueurs. La plupart des images sont la 
    propriété de Motion Twin.',
    'categories'        => 'Catégories',
    'recherche'         => 'Recherche',
    'toutes-categories' => 'toutes',
    'ajouter'           => 'ajouter',
    'ajouter-categorie' => 'Ajouter une catégorie',
    'editer-categorie'  => 'Editer une catégorie',
    'nom-categorie'     => 'Nom',
    'enregistrer'       => 'Enregistrer',
    'maj-en-cours'      => 'Mise à jour en cours, revenez dans quelques instants.'
];
