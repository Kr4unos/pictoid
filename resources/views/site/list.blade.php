@extends('layouts.app')

@section('title', 'Jeux')

@section('content')

    <?php
    $locales = Config::get('app.locales');
    ?>
    <ul class="nav nav-pills sites-lang-selector">
        <li><a href="" data-lang="" class="select-sites-by-lang">{{ ucfirst(trans('messages.tous_les_jeux')) }}</a></li>
        @foreach($locales as $code => $locale)
            <li><a href="" class="select-sites-by-lang" data-lang="{{ $code }}">{{ $locale['title'] }}</a></li>
        @endforeach
    </ul>
    <div class="row grid-sites">
        @foreach ($sites as $site)
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 box-site"
                 data-lang="{{ $site->lang ? $site->lang : 'all' }}">
                <div class="box box-solid">
                    @if($site->lang)
                        <i {!! Html::attributes(['class'=>'flag flag-'.$site->lang]) !!}></i>
                    @endif
                    <?php $user = $user_stats = $user_achievements = null?>
                    @if(!$site->users->isEmpty())
                        <?php $user = $site->users->first();
                        $user_stats = $user->statsSite();
                        $user_achievements = $user->achievementsSite();
                        ?>
                    @endif
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-4 img-jeu">
                                {!! Html::image($site->cover, $site->name,['class' => 'img-responsive', 'width'=>135, 'height' => 157]) !!}
                            </div>
                            <div class="col-xs-8 stats">
                                <h2 class="text-center box-title">{{$site->name}}</h2>
                                <ul class="list-unstyled">
                                    <?php $nb_stats = $site->statsCount; ?>
                                    <?php $nb_achievements = $site->achievementsCount; ?>
                                    <?php $nb_points = $site->pointsMax;?>
                                    <?php $nb_joueurs = $site->usersCount; ?>
                                    <li>{!! $user_stats ? '<span>'.$user_stats->count().'</span> / ' :'' !!}@choice('messages.nb_stats', $nb_stats, ['nb' => '<span>'.$nb_stats.'</span>'])</li>
                                    <li>{!! $user_achievements ? '<span>'.$user_achievements->count().'</span> / ' :'' !!}@choice('messages.nb_recompenses', $nb_achievements, ['nb' => '<span>'.$nb_achievements.'</span>'])</li>
                                    <li>{!! $user ? '<span>'.$user->points.'</span> / ' :'' !!}@choice('messages.nb_points', $nb_points, ['nb' => '<span>'.$nb_points.'</span>'])</li>
                                    <li>@choice('messages.nb_joueurs', $nb_joueurs, ['nb' => '<span>'.$nb_joueurs.'</span>'])</li>
                                </ul>
                            </div>
                            <div class="col-xs-12 footer">
                                <div class="progress sm">
                                    <?php
                                    $progressionGeneraleWidth = $site->progressionGenerale;
                                    ?>
                                    @if($user)
                                        <?php $progressionJoueur = $user->progression ?>
                                        <div class="progress-bar progress-bar-aqua"
                                             style="width:{{ $user->progression }}%;">
                                        </div>
                                        <?php $progressionGeneraleWidth -= $progressionJoueur; ?>
                                    @endif
                                    <div class="progress-bar general-progress"
                                         style="width: {{$progressionGeneraleWidth}}%;">
                                    </div>
                                </div>
                                <div class="buttons text-center">
                                    <a href="{{route('show-stats',['id' => $site->id])}}"
                                       class="btn btn-block btn-primary" type="button">@lang('messages.pictos')</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="col-xs-1 shuffle__sizer"></div>
    </div>
@endsection