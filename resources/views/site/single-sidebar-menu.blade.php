<li class="header">{{ucfirst(trans('messages.categories'))}}</li>
<li class="active">
    <a href="" class="category-filter" data-group="all">
        <span>{{ucfirst(trans('messages.toutes-categories'))}}</span>
    </a>
</li>
<?php
$categories = $site->categories->sortBy('name');
?>
@foreach( $categories as $cat)
    <?php
    $has_children = $cat->children->count() > 0;
    ?>
    <li class="{{$has_children ? 'treeview' : ''}}">
        <a href="#" class="category-filter"
           data-group="{{ str_slug($cat->name).'-'.$cat->id }}">
            @if($cat->icon)
                {!! Html::image($cat->icon, $cat->name) !!}
            @endif
            <span>{{ $cat->name }}</span>
            @permission('edit-category')
            <i class="fa fa-fw fa-pencil btn-edit-category"
               data-href="{{ route('category.edit', ['category' => $cat->id]) }}"></i>
            @endpermission
            @if($has_children)
                <i class="fa fa-angle-left pull-right"></i>
            @endif
        </a>
        @if($has_children)
            <?php $children = $cat->children->sortBy('name'); ?>
            <ul class="treeview-menu">
                @foreach($children as $child)
                    <li>
                        <a href="#" class="category-filter child" data-group="{{ str_slug($child->name).'-'.$child->id }}">
                            @if($child->icon)
                                {!! Html::image($child->icon, $child->name) !!}
                            @endif
                            <span>{{ $child->name }}</span>
                            @permission('edit-category')
                            <i class="fa fa-fw fa-pencil btn-edit-category"
                               data-href="{{ route('category.edit', ['category' => $child->id]) }}"></i>
                            @endpermission
                        </a>
                    </li>
                @endforeach
            </ul>
        @endif
    </li>
@endforeach
@permission('edit-category')
<li class="header">Admin</li>
<li>
    <a class="add-cat" href="#" data-site_id="{{$site->id}}"
       data-href="{{ route('category.create', ['site_id' => $site->id]) }}">
        <span>{{ucfirst(trans('messages.ajouter'))}}</span>
    </a>
</li>
@endpermission