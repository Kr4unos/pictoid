<aside class="main-sidebar">
    <section class="sidebar">
        @if(Auth::check())
            <div class="user-panel">
                <div class="image">
                    <img src="{{Auth::user()->avatar}}" class="img-circle" alt=""/>
                </div>
                <div class="info">
                    <p>{{Auth::user()->name}}</p>
                </div>
            </div>
        @endif
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control main-search" placeholder="{{trans('messages.recherche')}}">
                            <span class="input-group-btn">
                              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i
                                          class="fa fa-search"></i>
                              </button>
                            </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            @if(Route::is('show-stats'))
                @include('site.single-sidebar-menu')
            @endif
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>