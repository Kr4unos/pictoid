@if($achievement->type == App\Models\Achievement::TYPE_TITLE)
    {{$achievement->name}}
@elseif($achievement->type == App\Models\Achievement::TYPE_ICON)
    {!! Html::image($achievement->url,$achievement->title) !!}
@elseif($achievement->type == App\Models\Achievement::TYPE_IMAGE)
    {!! Html::image($achievement->url,$achievement->title) !!}
@else
    {{$achievement->name}} x{{$achievement->score}}
@endif