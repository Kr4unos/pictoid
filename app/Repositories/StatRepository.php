<?php
namespace App\Repositories;

use App\Models\Stat;

class StatRepository extends BaseRepository
{
    public function __construct(Stat $stat)
    {
        $this->model = $stat;
    }
}
