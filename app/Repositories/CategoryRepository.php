<?php
namespace App\Repositories;


use App\Models\Category;

class CategoryRepository extends BaseRepository
{
    public function __construct(Category $category)
    {
        $this->model = $category;
    }

    public function create($site_id, $name, $parent_id, $locale)
    {
        $category                                = new Category();
        $category->site_id                       = $site_id;
        $category->parent_id                     = $parent_id;
        $category->translateOrNew($locale)->name = $name;
        return $category->save();
    }

    public function getBySite($site_id)
    {
        return Category::where('site_id', $site_id)->get();
    }

    public function getCanBeParents($site_id)
    {
        return Category::select('categories.*')
                       ->join('category_translations as t', 't.category_id', '=', 'categories.id')
                       ->where('site_id', $site_id)
                       ->where('locale', \App::getLocale())
                       ->whereNull('parent_id')
                       ->orderBy('name', 'asc')
                       ->with('translations')
                       ->get();
    }
}
