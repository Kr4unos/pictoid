<?php
namespace App\Repositories;

use App\Models\Achievement;
use App\Models\Site;
use App\Models\Stat;
use App\Models\User;

class SiteRepository extends BaseRepository
{
    public function __construct(Site $site)
    {
        $this->model = $site;
    }

    public function getById($id, $user_id = 0)
    {
        $with = [
            'stats.users'              => function ($query) use ($user_id) {
                $query->where('id', '=', $user_id);
            },
            'stats.achievements.users' => function ($query) use ($user_id) {
                $query->where('id', '=', $user_id);
            },
            'categories'               => function ($query) {
                $query->whereNull('parent_id');
            }
        ];
        return Site::with($with)->findOrFail($id);
    }

    public function getAll($user_id = 0)
    {

        $with = ['usersCount', 'achievementsCount', 'statsCount', 'pointsMax'];

        $with['users'] = function ($query) use ($user_id) {
            $query->where('id', '=', $user_id);
        };
        return Site::with($with)->orderBy('name')->get();
    }
}