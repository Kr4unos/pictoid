<?php

namespace App\Models;

use App\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Stat
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Achievement[] $achievements
 */
class Stat extends Model
{
    use Translatable;
    protected $fillable = array('twino_id', 'name');
    public $translatedAttributes = ['name', 'description'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    public function users()
    {
        return $this->belongsToMany('App\Models\User')->withPivot('score');
    }
    public function sites()
    {
        return $this->belongsTo('App\Models\Site');
    }
    public function achievements()
    {
        return $this->hasMany('App\Models\Achievement')->orderBy('score');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category')->with('translations');
    }
}
