<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Acoustep\EntrustGui\Contracts\HashMethodInterface;
/**
 * App\Models\User
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Site[] $sites
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Stat[] $stats
 */
class User extends BaseModel implements AuthenticatableContract, AuthorizableContract, HashMethodInterface
{
    use Authenticatable, EntrustUserTrait;

    protected $fillable = ['name'];

    public $incrementing = false;

    public function sites()
    {
        return $this->belongsToMany('App\Models\Site');
    }

    public function stats()
    {
        return $this->belongsToMany('App\Models\Stat');
    }
    public function statsSite()
    {
        return $this->stats()
                        ->where('site_id', $this->pivot->site_id);
    }
    public function achievementsSite()
    {
        return $this->achievements()
                    ->where('site_id', $this->pivot->site_id);
    }

    public function achievements()
    {
        return $this->belongsToMany('App\Models\Achievement');
    }
    public function getPointsAttribute()
    {
        return (int)$this->pivot->npoints;
    }
    public function getProgressionAttribute()
    {
        return round($this->pivot->npoints / \Config::get('twinoid.points_max') * 100);
    }
    //non utilisé - utile uniquement pour que le package entrust-gui fonctionne système
    public function entrustPasswordHash()
    {
    }
}
