<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AchievementTranslation extends Model
{
    public $timestamps = false;
}
