<?php
function elixir($file)
{
    static $manifest = null;

    if (app()->isLocal()) {
        return asset($file);
    }

    if (is_null($manifest)) {
        $manifest = json_decode(file_get_contents(public_path('build/rev-manifest.json')), true);
    }

    if (isset($manifest[$file])) {
        return asset('/build/' . $manifest[$file]);
    }

    throw new InvalidArgumentException("File {$file} not defined in asset manifest.");
}
