<?php
namespace App\Traits;

trait Translatable
{
    use \Dimsav\Translatable\Translatable;

    /**
     * @param string|null $locale
     * @param bool        $withFallback
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function getTranslation($locale = null, $withFallback = null)
    {
        $configFallbackLocale = $this->getFallbackLocale($locale);
        $locale = $locale ?: $this->locale();
        $withFallback = $withFallback === null ? $this->useFallback() : $withFallback;
        $fallbackLocale = $this->getFallbackLocale($locale);

        if ($translation = $this->getTranslationByLocaleKey($locale)) {
            return $translation;
        }
        if ($withFallback && $fallbackLocale) {
            if ($translation = $this->getTranslationByLocaleKey($fallbackLocale)) {
                return $translation;
            }
            if ($translation = $this->getTranslationByLocaleKey($configFallbackLocale)) {
                return $translation;
            }
            if (!$this->translations->isEmpty()) {
                return $this->translations->first();
            }
        }
        return null;
    }
}
