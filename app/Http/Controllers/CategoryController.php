<?php

namespace App\Http\Controllers;

use App;
use App\Repositories\CategoryRepository;
use \Binput;
use Illuminate\Http\Request;

use App\Http\Requests;

class CategoryController extends Controller
{
    /**
     * @var \App\Repositories\CategoryRepository
     */
    private $categories;

    public function __construct(CategoryRepository $categories)
    {
        $this->categories = $categories;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            $this->validate(
                $request,
                [
                    'site_id' => 'exists:sites,id'
                ]
            );
            $categories = $this->categories->getCanBeParents($request->input('site_id'));
            return view('category.edit', ['site_id' => $request->input('site_id'), 'categories' => $categories]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            $request->offsetSet("name", Binput::get('name'));
            $this->validate(
                $request,
                [
                    'name'      => 'required',
                    'site_id'   => 'bail|required|exists:sites,id',
                    'parent_id' => 'exists:categories,id'
                ]
            );
            return response()->json(
                $this->categories->create(
                    (int)$request->input('site_id'),
                    $request->input('name'),
                    !empty($request->input('parent_id')) ? (int)$request->input('parent_id') : null,
                    App::getLocale()
                )
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            $category   = $this->categories->getById($id);
            $categories = $this->categories->getCanBeParents($category->site_id)->keyBy('id');
            debug($this->categories->getCanBeParents($category->site_id)->keyBy('id')->pluck('name', 'id')->toArray());
            $categories->forget($category->id);
            return view(
                'category.edit',
                [
                    'categories' => $categories,
                    'category'   => $category
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->ajax()) {
            $request->offsetSet("name", Binput::get('name'));
            $request->offsetSet("parent_id", Binput::get('parent_id'));
            $this->validate(
                $request,
                [
                    'name'        => 'required',
                    'category_id' => 'exists:categories,id',
                    'parent_id'   => 'exists:categories,id'
                ]
            );
            $cat       = $this->categories->getById($id);
            $cat->name = $request->input('name');
            if (!empty($request->input('parent_id'))) {
                $cat->parent_id = (int)$request->input('parent_id');
            } else {
                $cat->parent_id = null;
            }
            return response()->json(
                $cat->save()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
