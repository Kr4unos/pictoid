<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use \Auth as Auth;
use App\Http\Requests;
use App\Services\TwinoidAPI;

class UserController extends Controller
{
    /**
     * @var \App\Repositories\UserRepository
     */
    private $users;

    public function __construct(UserRepository $users)
    {
        $this->middleware('auth');
        $this->users = $users;
    }

    public function showProfile()
    {
        return view('user.profile', []);
    }
    //route disabled
    public function showContacts()
    {
        return view('user.contacts', []);
    }

    //route disabled
    public function ajaxGetContactsId(Request $request)
    {
        $twinoidAPI = new TwinoidAPI($request->session()->get('twinoid_token'));
        $user       = $twinoidAPI->getContacts();
        return response()->json($user->contacts);
    }

    //route disabled
    public function ajaxAddContactUser(Request $request)
    {
        $hours_up_to_date = 24;
        $twinoidAPI       = new TwinoidAPI($request->session()->get('twinoid_token'));
        $id               = $request->input('id');
        if ($id) {
            if (UserRepository::isUserUpToDate($id, $hours_up_to_date)) {
                $return = trans('messages.contact_already_up_to_date', ['hours' => $hours_up_to_date]);
            } else {
                $user   = $this->users->updateOrCreate($twinoidAPI->getUser($id), $hours_up_to_date);
                $return = 'OK';
            }
            return response()->json($return);
        }
    }

    public function ajaxCheckDataJob(Request $request)
    {
        if (Auth::check() && \Cache::has('userdata_job_'.Auth::user()->id)) {
            return response()->json(true);
        } else {
            return response()->json(false);
        }
    }
}
