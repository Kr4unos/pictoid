<?php

namespace App\Http\Controllers;

use App\Models\Stat;
use App\Repositories\CategoryRepository;
use App\Repositories\StatRepository;
use Illuminate\Http\Request;

use App\Http\Requests;

class StatController extends Controller
{
    /**
     * @var \App\Repositories\StatRepository
     */
    private $stats;
    /**
     * @var \App\Repositories\CategoryRepository
     */
    private $categories;

    public function __construct(StatRepository $stats, CategoryRepository $categories)
    {
        $this->stats      = $stats;
        $this->categories = $categories;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        if ($request->ajax()) {
            $stat       = $this->stats->getById($id);
            $categories = $this->categories->getBySite($stat->site_id);
            return view('stat.edit', ['stat' => $stat, 'categories' => $categories]);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /**
         * @var \App\Models\Stat
         */
        $stat = $this->stats->getById($id);
        $this->validate(
            $request,
            [
                'category' => 'exists:categories,id,site_id,'.$stat->site_id
            ]
        );
        $category = $this->categories->getById($request->category);
        $stat->category()->associate($category);
        $stat->save();
        return response()->json(
            [
                'name' => $category->name,
                'slug' => $category->slug
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
