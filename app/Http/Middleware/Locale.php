<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Config;
use Request;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->ajax()) {
            $locale = Request::header('X-PICTOID-LOCALE');
            if (!array_key_exists($locale, Config::get('laravellocalization.supportedLocales'))) {
                $locale = Config::get('app.locale');
            }
            App::setLocale($locale);
        }

        return $next($request);
    }
}
