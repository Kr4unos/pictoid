var elixir = require( 'laravel-elixir' );
var gulp = require( 'gulp' );
var shell = require( 'gulp-shell' );
var inProduction = elixir.config.production;

var Task = elixir.Task;

elixir.extend( 'langjs', function( path ) {
    new Task( 'langjs', function() {
        gulp.src( '' ).pipe( shell( 'php artisan lang:js ' + ( path || 'public/assets/tmp/js/messages.js' ) ) );
    } ).watch( './resources/**/*.php' );
} );
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.app
 |
 */

elixir( function( mix ) {
    mix.langjs();
    mix.sass(
        [
            'app.scss',
            '../vendor/AdminLTE/dist/css/AdminLTE.css',
            '../vendor/load-awesome/css/ball-clip-rotate-pulse.css'
        ],
        'public/assets/css/pictoid.css' );
    mix.scripts(
        [

            '../vendor/AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js',
            '../vendor/AdminLTE/bootstrap/js/bootstrap.min.js',
            '../vendor/shufflejs/dist/jquery.shuffle.modernizr.min.js',
            '../vendor/AdminLTE/dist/js/app.js',
            '../vendor/bootbox.js/bootbox.js',
            '../vendor/laroute/laroute.js',
            './public/assets/tmp/js/messages.js',
            'config.js',
            'components/*.js',
            'pages/*.js'
        ],
        'public/assets/js/pictoid.js' );

    mix.version( ['assets/css/pictoid.css', 'assets/js/pictoid.js'] );

    mix.copy( 'resources/assets/img', 'public/assets/img/' );

    mix.browserSync( {
        files: [
            'public/assets/css/*.css',
            'public/assets/js/*.js',
            'resources/views/**/*.blade.php',
            'app/**/*.php'
        ],
        injectChanges: true,
        open: false,
        proxy: {
            target: 'pictoid.app'
        },
        reloadOnRestart: false,
        snippetOptions: {
            whitelist: ['/stat/**', 'category/**'] // Pour que les urls contenues dans les réponses ajax soient remplacées. Je ne mets pas toutes les url en utilisant des asterisques car cela fait planter les images.
        }
    } );
} );
