<?php
return [
    "layout" => "entrust-gui::app",
    "route-prefix" => "admin-roles",
    "pagination" => [
        "users" => 5,
        "roles" => 5,
        "permissions" => 5,
    ],
    "middleware" => ['web' ,'entrust-gui.admin'],
    "unauthorized-url" => '/',
    "middleware-role" => 'admin',
    "confirmable" => false,
];
