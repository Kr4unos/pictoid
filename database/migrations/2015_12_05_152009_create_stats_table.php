<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('twino_id',100);
            $table->string('icon')->nullable();
            $table->integer('score');
            $table->integer('site_id')->unsigned()->index();
            $table->foreign('site_id')->references('id')->on('sites');
            $table->integer('category')->unsigned()->nullable();
            $table->unique(array('twino_id', 'site_id'));
            $table->timestamps();
        });
        Schema::create('stat_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stat_id')->unsigned()->index();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('locale',10)->index();

            $table->unique(['stat_id', 'locale']);
            $table->foreign('stat_id')->references('id')->on('stats')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stat_translations');
        Schema::drop('stats');
    }
}
