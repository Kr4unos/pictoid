<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('icon')->nullable();
            $table->timestamps();
        });
        Schema::create('category_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned()->index();
            $table->string('name');
            $table->string('locale', 10)->index();

            $table->unique(['category_id', 'locale']);
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });

        DB::table('categories')
          ->insert(
              [
                  'id'         => 1,
                  'icon'       => 'http://data.twinoid.com/img/icons/new.png',
                  'created_at' => date('Y-m-d H:i:s'),
                  'updated_at' => date('Y-m-d H:i:s'),
              ]
          );
        DB::table('category_translations')
          ->insert(
              [
                  'category_id' => 1,
                  'name'        => 'Nouveaux',
                  'locale'      => 'fr'
              ]
          );
        Schema::table('stats', function ($table) {
            $table->dropColumn('category');
            $table->integer('category_id')->unsigned()->default(1);
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('restrict');
        });
        DB::table('stats')->update(['category_id' => 1]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stats', function ($table) {
            $table->integer('category')->unsigned()->nullable();
            $table->dropForeign('stats_category_id_foreign');
            $table->dropColumn('category_id');
        });
        Schema::drop('category_translations');
        Schema::drop('categories');
    }
}
